function tinhDiemKhuVuc(khuVuc) {
  switch (khuVuc) {
    case "A":
      return 2;
    case "B":
      return 1;
    case "C":
      return 0.5;
    default:
      return 0;
  }
}
function tinhDiemDoiTuong(doiTuong) {
  switch (doiTuong) {
    case "1":
      return 2.5;
    case "2":
      return 1.5;
    case "3":
      return 1;
    default:
      return 0;
  }
}
function tinhKetQua(
  diemChuan,
  diemMon_1,
  diemMon_2,
  diemMon_3,
  diemDoiTuong,
  diemKhuVuc
) {
  var diemTong = diemMon_1 + diemMon_2 + diemMon_3 + diemDoiTuong + diemKhuVuc;
  if (diemMon_1 <= 0 || diemMon_2 <= 0 || diemMon_3 <= 0) {
    result = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
  } else if (diemTong < diemChuan) {
    result = `Bạn đã rớt. Tổng điểm: ${diemTong}`;
  } else {
    result = `Bạn đã đậu. Tổng điểm: ${diemTong}`;
  }
  return result;
}
function ketQuaTuyenSinh() {
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVucVal = document.getElementById("txt-khu-vuc").value;
  var doiTuongVal = document.getElementById("txt-doi-tuong").value;
  var diemMon_1 = document.getElementById("txt-mon-1").value * 1;
  var diemMon_2 = document.getElementById("txt-mon-2").value * 1;
  var diemMon_3 = document.getElementById("txt-mon-3").value * 1;
  var diemKhuVuc = tinhDiemKhuVuc(khuVucVal);
  var diemDoiTuong = tinhDiemDoiTuong(doiTuongVal);
  ketQua = tinhKetQua(
    diemChuan,
    diemMon_1,
    diemMon_2,
    diemMon_3,
    diemDoiTuong,
    diemKhuVuc
  );
  document.getElementById("result_1").innerHTML = ketQua;
}
// BÀI 2
const tienDien1_50Kw = 500;
const tienDien51_100Kw = 650;
const tienDien101_200Kw = 850;
const tienDien201_350Kw = 1100;
const tienDien351KwTroLen = 1300;

function tinhTienDienTieuThu(soDienTieuThu) {
  if (soDienTieuThu <= 50) {
    tongTien = soDienTieuThu * tienDien1_50Kw;
  } else if (soDienTieuThu <= 100) {
    tongTien = 50 * tienDien1_50Kw + (soDienTieuThu - 50) * tienDien51_100Kw;
  } else if (soDienTieuThu <= 200) {
    tongTien =
      50 * (tienDien1_50Kw + tienDien51_100Kw) +
      (soDienTieuThu - 100) * tienDien101_200Kw;
  } else if (soDienTieuThu <= 350) {
    tongTien =
      50 * (tienDien1_50Kw + tienDien51_100Kw) +
      100 * tienDien101_200Kw +
      (soDienTieuThu - 150) * tienDien201_350Kw;
  } else if (soDienTieuThu > 350) {
    tongTien =
      50 * (tienDien1_50Kw + tienDien51_100Kw) +
      100 * tienDien101_200Kw +
      150 * tienDien201_350Kw +
      (soDienTieuThu - 350) * tienDien351KwTroLen;
  } else return alert("Số kw không hợp lệ! Vui lòng nhập lại");
  return tongTien;
}
function thongBaoTienDien() {
  var tenKhachHang = document.getElementById("txt-name").value;
  var soDienTieuThu = parseFloat(document.getElementById("txt-kw").value);
  var tongTien = tinhTienDienTieuThu(soDienTieuThu);
  document.getElementById("result_2").innerHTML = `
  <p>Mr/Ms: ${tenKhachHang}</p>
  <p>Tiền điện: ${new Intl.NumberFormat("vn-VN").format(tongTien)}</p>`;
}
